package gui;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class GUILogin extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPanel;
	private JTextField emailField;
	private JTextField passwordField;
	private JComboBox userComboBox;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUILogin frame = new GUILogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public JComboBox<String> getUserComboBox() {
		return getUserComboBox();
	}

	public GUILogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("User Login");
		setBounds(100, 100, 700, 670);
		contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPanel);
		contentPanel.setLayout(null);

		JLabel emailLabel = new JLabel("email");
		emailLabel.setBounds(5, 11, 71, 14);
		contentPanel.add(emailLabel);

		JLabel passwordLabel = new JLabel("password");
		passwordLabel.setBounds(5, 36, 71, 14);
		contentPanel.add(passwordLabel);
		
		String[] Strings = {"admin", "teacher", "parent", "student"};
		JComboBox userComboBox = new JComboBox(Strings);
		userComboBox.setBounds(5, 66, 109, 23);
		contentPanel.add(userComboBox);

		emailField = new JTextField();
		emailField.setBounds(80, 10, 100, 20);
		contentPanel.add(emailField);
		emailField.setColumns(10);

		passwordField = new JTextField();
		passwordField.setBounds(80, 35, 100, 20);
		contentPanel.add(passwordField);
		passwordField.setColumns(10);

		JButton buttonLogin = new JButton("Login");
		buttonLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {	
			String role = (String) userComboBox.getSelectedItem();
					Client client = new Client("localhost", 8986);
					try {
						client.connect();
						switch (role) {
						case "teacher":
							client.writeMessage("teacherLogin:"+ emailField.getText() + ":"+ passwordField.getText());
							break;
						case "parent":
							client.writeMessage("parentLogin:"+ emailField.getText() + ":"+ passwordField.getText());
							break;
						case "student":
							client.writeMessage("studentLogin:"+ emailField.getText() + ":"+ passwordField.getText());
							break;
						case "admin": 
							client.writeMessage("adminLogin:"+ emailField.getText() + ":"+ passwordField.getText());
							break;
						}
						client.readResponse();
					} catch (IOException e) {
						System.err.println("Can't connect!" + e.getMessage());
					}	
			}
		});
		buttonLogin.setBounds(5, 120, 90, 25);
		contentPanel.add(buttonLogin);
	}
}
