package gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.Class;
import model.Grade;
import model.User;
import model.ViewTable;

public class GUITeacher extends JFrame{
	private JPanel contentPanel;
	private JTable userTable;
	private JTable classTable;
	private JTable gradeTable;
	private JTextField idField;
	private JTextField userField;
	private JTextField passField;
	private JTextField nameField;
	private JTextField dayField;
	private JTextField hourField;
	private JTextField usernameField;
	
	private JTextField idcField;
	private JTextField c_nameField;
	private JTextField c_dayField;
	private JTextField c_hourField;
	private JTextField c_teacherField;
	
	private JTextField idgField;
	private JTextField g_teacherField;
	private JTextField g_studentField;
	private JTextField g_classField;
	private JTextField g_finalField;
	private JTextField g_valueField;
	
	
	private JButton btnUpdate;
	private JButton btnDelete;
	private JComboBox<String> userComboBox;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	 
	public GUITeacher() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Teacher area");
		setBounds(100, 100, 700, 670);
		
		contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 11, 561, 326);
		tabbedPane.setBounds(0, 11, 600, 620);
		contentPanel.add(tabbedPane);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Schedule", null, panel_1, null);
		panel_1.setLayout(null);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 11, 536, 128);
		panel_1.add(scrollPane_1);
		
		classTable = new JTable();
		classTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = classTable.getSelectedRow();

				idcField.setText(classTable.getModel().getValueAt(row, 0).toString());
				c_nameField.setText(classTable.getModel().getValueAt(row, 1).toString());
				c_dayField.setText(classTable.getModel().getValueAt(row, 2).toString());
				c_hourField.setText(classTable.getModel().getValueAt(row, 3).toString());
				c_teacherField.setText(classTable.getModel().getValueAt(row, 4).toString());
			}
		});
		scrollPane_1.setViewportView(classTable);
		
		JLabel lblUsername = new JLabel("ID");
		lblUsername.setBounds(10, 445, 66, 14);
		panel_1.add(lblUsername);
		
		usernameField = new JTextField();
		usernameField.setBounds(59, 445, 110, 20);
		panel_1.add(usernameField);
		usernameField.setColumns(10);
		
		JButton btnAdd = new JButton("Schedule"); 
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client client = new Client("localhost", 8986);
				try {
					client.connect();
					//client.writeMessage("viewTeacherClass:"+usernameField.getText());
					client.writeMessage("getClassList");
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!"+ e1.getMessage());
				}
				
				initClasses(usernameField.getText());
				
			}
		});
		btnAdd.setBounds(197, 545, 89, 23);
		panel_1.add(btnAdd);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Assign grades to students", null, panel_2, null);
		panel_2.setLayout(null);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(10, 11, 536, 128);
		panel_2.add(scrollPane_2);
		
		gradeTable = new JTable();
		gradeTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = gradeTable.getSelectedRow();

				idgField.setText(gradeTable.getModel().getValueAt(row, 0).toString());
				g_teacherField.setText(gradeTable.getModel().getValueAt(row, 1).toString());
				g_studentField.setText(gradeTable.getModel().getValueAt(row, 2).toString());
				g_classField.setText(gradeTable.getModel().getValueAt(row, 3).toString());
				g_finalField.setText(gradeTable.getModel().getValueAt(row, 4).toString());
				g_valueField.setText(gradeTable.getModel().getValueAt(row, 5).toString());
			}
		});
		scrollPane_2.setViewportView(gradeTable);
		
		JLabel lblId_2 = new JLabel("ID");
		lblId_2.setBounds(10, 445, 66, 14);
		panel_2.add(lblId_2);
		
		JLabel lblClass = new JLabel("Teacher");
		lblClass.setBounds(10, 470, 66, 14);
		panel_2.add(lblClass);
		
		JLabel lblStudent = new JLabel("Student");
		lblStudent.setBounds(10, 495, 66, 14);
		panel_2.add(lblStudent);
		
		JLabel lblClassname = new JLabel("Class");
		lblClassname.setBounds(10, 520, 66, 14);
		panel_2.add(lblClassname);
		
		JLabel lblFinal = new JLabel("Final");
		lblFinal.setBounds(10, 545, 66, 14);
		panel_2.add(lblFinal);
		
		JLabel lblValue = new JLabel("Value");
		lblValue.setBounds(10, 570, 66, 14);
		panel_2.add(lblValue);
		
		idgField = new JTextField();
		idgField.setBounds(59, 445, 110, 20);
		panel_2.add(idgField);
		idgField.setColumns(10);
		
		g_teacherField = new JTextField();
		g_teacherField.setBounds(59, 470, 110, 20);
		panel_2.add(g_teacherField);
		g_teacherField.setColumns(10);
		
		g_studentField = new JTextField();
		g_studentField.setBounds(59, 495, 110, 20);
		panel_2.add(g_studentField);
		g_studentField.setColumns(10);
		
		g_classField = new JTextField();
		g_classField.setBounds(59, 520, 110, 20);
		panel_2.add(g_classField);
		g_classField.setColumns(10);
		
		g_finalField = new JTextField();
		g_finalField.setBounds(59, 545, 110, 20);
		panel_2.add(g_finalField);
		g_finalField.setColumns(10);
		
		g_valueField = new JTextField();
		g_valueField.setBounds(59, 570, 110, 20);
		panel_2.add(g_valueField);
		g_valueField.setColumns(10);
		
		
		
		JButton btnAssign = new JButton("Assign"); 
		btnAssign.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client client = new Client("localhost", 8986); 
				Grade g=new Grade();
				g.setId(Integer.parseInt(idgField.getText()));
				g.setTeacher_id(Integer.parseInt(g_teacherField.getText()));
				g.setStudent_id(Integer.parseInt(g_studentField.getText()));
				g.setClassname(g_classField.getText());
				g.setIs_final(Integer.parseInt(g_finalField.getText()));
				g.setValue(Integer.parseInt(g_valueField.getText()));
				client.addGrade(g);
				try {
					client.connect();
					client.writeMessage("addGrade:"+idgField.getText()+":"+g_teacherField.getText()+":"
						+g_studentField.getText() +":" +g_classField.getText()+":" +g_finalField.getText() +":" +g_valueField.getText());
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!"+ e1.getMessage());
				}
				
				init();
				
			}
		});
		btnAssign.setBounds(197, 545, 89, 23);
		panel_2.add(btnAssign);
		
		JButton btnEdit = new JButton("Edit"); 
		btnEdit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client client = new Client("localhost", 8986); 
				Grade g=new Grade();
				g.setId(Integer.parseInt(idgField.getText()));
				g.setTeacher_id(Integer.parseInt(g_teacherField.getText()));
				g.setStudent_id(Integer.parseInt(g_studentField.getText()));
				g.setClassname(g_classField.getText());
				g.setIs_final(Integer.parseInt(g_finalField.getText()));
				g.setValue(Integer.parseInt(g_valueField.getText()));
				client.updateGrade(g);
				try {
					client.connect();
					client.writeMessage("updateGrade:"+idgField.getText()+":"+g_teacherField.getText()+":"
							+g_studentField.getText() +":" +g_classField.getText()+":" +g_finalField.getText() +":" +g_valueField.getText());
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!"+ e1.getMessage());
				}
				
				init();
				
			}
		});
		btnEdit.setBounds(290, 545, 89, 23);
		panel_2.add(btnEdit);
		
		
		
		init();
	}
	
	private void initClasses(String id) {
		Client client = new Client("localhost", 8986);
		
		try {
			client.connect();
			client.writeMessage("getClassList");
			//client.writeMessage("viewTeacherClass:"+id);
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		classTable.setModel(ViewTable.classesToTableModel(client.getTeacherClasses(id)));
	}
	
	private void init() {
		Client client = new Client("localhost", 8986);
		
		try {
			client.connect();
			client.writeMessage("getClassList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		classTable.setModel(ViewTable.classesToTableModel(client.getClasses()));
		
		try {
			client.connect();
			client.writeMessage("getGradeList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		gradeTable.setModel(ViewTable.gradesToTableModel(client.getGrades()));
	}
}
