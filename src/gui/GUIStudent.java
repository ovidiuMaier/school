package gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.Grade;
import model.ViewTable;

public class GUIStudent extends JFrame{
	private JPanel contentPanel;
	private JTable linkTable;
	private JTable classTable;
	private JTable gradeTable;
	
	private JTextField usernameField;
	private JTextField username2Field;
	
	private JTextField idcField;
	private JTextField c_nameField;
	private JTextField c_dayField;
	private JTextField c_hourField;
	private JTextField c_teacherField;
	
	private JTextField idgField;
	private JTextField g_teacherField;
	private JTextField g_studentField;
	private JTextField g_classField;
	private JTextField g_finalField;
	private JTextField g_valueField;
	
	public GUIStudent() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Student area");
		setBounds(100, 100, 700, 670);
		
		contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 11, 561, 326);
		tabbedPane.setBounds(0, 11, 600, 620);
		contentPanel.add(tabbedPane);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Schedule", null, panel_1, null);
		panel_1.setLayout(null);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 11, 536, 128);
		panel_1.add(scrollPane_1);
		
		
		linkTable = new JTable();
		linkTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = linkTable.getSelectedRow();

//				idlField.setText(linkTable.getModel().getValueAt(row, 0).toString());
//				l_classField.setText(linkTable.getModel().getValueAt(row, 1).toString());
//				l_studentField.setText(linkTable.getModel().getValueAt(row, 2).toString());
			}
		});
		
		classTable = new JTable();
		classTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = classTable.getSelectedRow();

				idcField.setText(classTable.getModel().getValueAt(row, 0).toString());
				c_nameField.setText(classTable.getModel().getValueAt(row, 1).toString());
				c_dayField.setText(classTable.getModel().getValueAt(row, 2).toString());
				c_hourField.setText(classTable.getModel().getValueAt(row, 3).toString());
				c_teacherField.setText(classTable.getModel().getValueAt(row, 4).toString());
			}
		});
		scrollPane_1.setViewportView(classTable);
		
		JLabel lblUsername = new JLabel("ID");
		lblUsername.setBounds(10, 445, 66, 14);
		panel_1.add(lblUsername);
		
		usernameField = new JTextField();
		usernameField.setBounds(59, 445, 110, 20);
		panel_1.add(usernameField);
		usernameField.setColumns(10);
		
		JButton btnAdd = new JButton("Schedule"); 
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client client = new Client("localhost", 8986);
				try {
					client.connect();
					//client.writeMessage("viewTeacherClass:"+usernameField.getText());
					client.writeMessage("getClassList");
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!"+ e1.getMessage());
				}
				
				initClasses(usernameField.getText());
				
			}
		});
		btnAdd.setBounds(197, 545, 89, 23);
		panel_1.add(btnAdd);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Grades", null, panel_2, null);
		panel_2.setLayout(null);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(10, 11, 536, 128);
		panel_2.add(scrollPane_2);
		
		
		JLabel lblUsername2 = new JLabel("ID");
		lblUsername2.setBounds(10, 445, 66, 14);
		panel_2.add(lblUsername2);
		
		username2Field = new JTextField();
		username2Field.setBounds(59, 445, 110, 20);
		panel_2.add(username2Field);
		username2Field.setColumns(10);
		
		gradeTable = new JTable();
		gradeTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = gradeTable.getSelectedRow();

				idgField.setText(gradeTable.getModel().getValueAt(row, 0).toString());
				g_teacherField.setText(gradeTable.getModel().getValueAt(row, 1).toString());
				g_studentField.setText(gradeTable.getModel().getValueAt(row, 2).toString());
				g_classField.setText(gradeTable.getModel().getValueAt(row, 3).toString());
				g_finalField.setText(gradeTable.getModel().getValueAt(row, 4).toString());
				g_valueField.setText(gradeTable.getModel().getValueAt(row, 5).toString());
			}
		});
		scrollPane_2.setViewportView(gradeTable);
		
		
		JButton btnGrades = new JButton("Grades"); 
		btnGrades.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client client = new Client("localhost", 8986);
				try {
					client.connect();
					//client.writeMessage("viewTeacherClass:"+usernameField.getText());
					client.writeMessage("getGradeList");
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!"+ e1.getMessage());
				}
				
				initGrades(username2Field.getText());
				
			}
		});
		btnGrades.setBounds(197, 545, 89, 23);
		panel_2.add(btnGrades);
			
		
		init();
	}
	
	
	private void initGrades(String id) {
		Client client = new Client("localhost", 8986);
		
		try {
			client.connect();
			client.writeMessage("getGradeList");
			//client.writeMessage("viewTeacherClass:"+id);
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		
		gradeTable.setModel(ViewTable.gradesToTableModel(client.getStudentGrades(id)));
	}
	
	
	private void initClasses(String id) {
		Client client = new Client("localhost", 8986);
		
		try {
			client.connect();
			client.writeMessage("getClassList");
			//client.writeMessage("viewTeacherClass:"+id);
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		
		try {
			client.connect();
			client.writeMessage("getLinkList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		
		classTable.setModel(ViewTable.classesToTableModel(client.getStudentClasses(id)));
	}
	
	private void init() {
		Client client = new Client("localhost", 8986);
		
		try {
			client.connect();
			client.writeMessage("getClassList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		classTable.setModel(ViewTable.classesToTableModel(client.getClasses()));
		
		try {
			client.connect();
			client.writeMessage("getLinkList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		linkTable.setModel(ViewTable.linksToTableModel(client.getLinks()));
		
		
		try {
			client.connect();
			client.writeMessage("getGradeList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		gradeTable.setModel(ViewTable.gradesToTableModel(client.getGrades()));
	}
}
