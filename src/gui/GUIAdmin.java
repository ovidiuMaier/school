package gui;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.*;
import model.Class;

import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ButtonGroup;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GUIAdmin extends JFrame {

	private JPanel contentPanel;
	private JTable userTable;
	private JTable classTable;
	private JTable linkTable;
	private JTextField idField;
	private JTextField userField;
	private JTextField passField;
	private JTextField nameField;
	private JTextField dayField;
	private JTextField hourField;
	
	private JTextField idcField;
	private JTextField c_nameField;
	private JTextField c_dayField;
	private JTextField c_hourField;
	private JTextField c_teacherField;
	
	private JTextField idlField;
	private JTextField l_classField;
	private JTextField l_studentField;
	
	
	private JButton btnUpdate;
	private JButton btnDelete;
	private JComboBox<String> userComboBox;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	 
	public GUIAdmin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Admin area");
		setBounds(100, 100, 700, 670);
		
		contentPanel = new JPanel();
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPanel);
		contentPanel.setLayout(null);
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 11, 561, 326);
		tabbedPane.setBounds(0, 11, 600, 620);
		contentPanel.add(tabbedPane);

		
//		JScrollPane scrollPane = new JScrollPane();
//		scrollPane.setBounds(10, 11, 670, 400);
//		contentPanel.add(scrollPane);
//		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Users", null, panel, null);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();		
		
		scrollPane.setBounds(10, 11, 536, 151);
		panel.add(scrollPane);


		
		String[] Strings = {"student", "teacher"};
		JComboBox userComboBox = new JComboBox(Strings);
		userComboBox.setBounds(86, 545, 86, 20);
		panel.add(userComboBox);
		
		userTable = new JTable();
		userTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = userTable.getSelectedRow();

				idField.setText(userTable.getModel().getValueAt(row, 0).toString());
				userField.setText(userTable.getModel().getValueAt(row, 1).toString());
				passField.setText(userTable.getModel().getValueAt(row, 2).toString());	
				String type=userTable.getModel().getValueAt(row, 3).toString();
				
				System.out.println("tip:"+type+":");
				switch(type)
				{
				case "teacher":
					userComboBox.setSelectedItem("teacher");break;
				case "student":
					userComboBox.setSelectedItem("student");break;
				}
				
			}
		});
		scrollPane.setViewportView(userTable);
		
		JLabel labelId = new JLabel("ID");
		labelId.setBounds(10, 470, 46, 14);
		panel.add(labelId);
		
		JLabel lblUser = new JLabel("Email");
		lblUser.setBounds(10, 495, 46, 14);
		panel.add(lblUser);
		
		JLabel labelPassword = new JLabel("Password");
		labelPassword.setBounds(10, 520, 66, 14);
		panel.add(labelPassword);
		
		JLabel labelRole = new JLabel("Role");
		labelRole.setBounds(10, 545, 66, 14);
		panel.add(labelRole);
		
		
		idField = new JTextField();
		idField.setBounds(86, 470, 86, 20);
		panel.add(idField);
		idField.setColumns(10);
		
		userField = new JTextField();
		userField.setBounds(86, 495, 86, 20);
		panel.add(userField);
		userField.setColumns(10);
		
		passField = new JTextField();
		passField.setBounds(86, 520, 86, 20);
		panel.add(passField);
		passField.setColumns(10);
		
		
		
		JButton btnCreate = new JButton("Create");
		btnCreate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Client client = new Client("localhost", 8986); 
			
				User u=new User(userField.getText(),passField.getText());
				u.setId(Integer.parseInt(idField.getText()));
				if(userComboBox.getSelectedItem() == "teacher")
					{	
						u.setRole("teacher");
						try {
							client.connect();
							client.writeMessage("addTeacher:"+idField.getText()+":"
												+userField.getText()+":"+passField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.addUser(u);
					}
				else if(userComboBox.getSelectedItem() == "student")
					{
						u.setRole("student");
						try {
							client.connect();
							client.writeMessage("addStudent:"+idField.getText()+":"
												+userField.getText()+":"+passField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.addUser(u);
					}
				else if(userComboBox.getSelectedItem() == "admin")
				{
					try {
						client.connect();
						client.writeMessage("addAdmin:"+idField.getText()+":"
											+userField.getText()+":"+passField.getText());
						client.readResponse();
					} catch (IOException e1) {
						System.err.println("Can't connect!"+ e1.getMessage());
					}
				}
									
				init();
			}
		});
		btnCreate.setBounds(197, 545, 89, 23);
		panel.add(btnCreate);
		
		btnUpdate = new JButton("Update");
		btnUpdate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Client client = new Client("localhost", 8986); 
				 
				User u=new User(userField.getText(),passField.getText());
				u.setId(Integer.parseInt(idField.getText()));
				if(userComboBox.getSelectedItem() == "teacher")
					{	
						u.setRole("teacher");
						try {
							client.connect();
							client.writeMessage("updateTeacher:"+idField.getText()+":"
												+userField.getText()+":"+passField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.updateUser(u);
					}
				else if(userComboBox.getSelectedItem() == "student")
					{
						u.setRole("student");
						try {
							client.connect();
							client.writeMessage("updateStudent:"+idField.getText()+":"
												+userField.getText()+":"+passField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.updateUser(u);
					}
				
									
				init();
				
			}
		});
		btnUpdate.setBounds(290, 545, 89, 23);
		panel.add(btnUpdate);
		
		btnDelete = new JButton("Delete");
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Client client = new Client("localhost", 8986); 
				 
				User u=new User(userField.getText(),passField.getText());
				u.setId(Integer.parseInt(idField.getText()));
				if(userComboBox.getSelectedItem() == "teacher")
					{	
						u.setRole("teacher");
						try {
							client.connect();
							client.writeMessage("deleteTeacher:"+idField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.deleteUser(u);
					}
				else if(userComboBox.getSelectedItem() == "student")
					{
						u.setRole("student");
						try {
							client.connect();
							client.writeMessage("deleteStudent:"+idField.getText());
							client.readResponse();
						} catch (IOException e1) {
							System.err
									.println("Can't connect!"+ e1.getMessage());
						}
						client.deleteUser(u);
					}
				
									
				init();
				
			}
		});
		btnDelete.setBounds(383, 545, 89, 23);
		panel.add(btnDelete);
		
		JButton btnNotify = new JButton("Arrived");
		btnNotify.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Client client = new Client("localhost",  8986); 
				
				try {
					client.connect();
					client.writeMessage("arrived");
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!" + e1.getMessage());
				}
				
				
			}
		});
		btnNotify.setBounds(500, 545, 89, 23);
		panel.add(btnNotify);

		
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Classes", null, panel_1, null);
		panel_1.setLayout(null);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 11, 536, 128);
		panel_1.add(scrollPane_1);
		
		classTable = new JTable();
		classTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = classTable.getSelectedRow();

				idcField.setText(classTable.getModel().getValueAt(row, 0).toString());
				c_nameField.setText(classTable.getModel().getValueAt(row, 1).toString());
				c_dayField.setText(classTable.getModel().getValueAt(row, 2).toString());
				c_hourField.setText(classTable.getModel().getValueAt(row, 3).toString());
				c_teacherField.setText(classTable.getModel().getValueAt(row, 4).toString());
			}
		});
		scrollPane_1.setViewportView(classTable);
		
		JLabel lblId_1 = new JLabel("ID");
		lblId_1.setBounds(10, 445, 66, 14);
		panel_1.add(lblId_1);
		
		JLabel lblName = new JLabel("Name");
		lblName.setBounds(10, 470, 66, 14);
		panel_1.add(lblName);
		
		JLabel lblDay = new JLabel("Day");
		lblDay.setBounds(10, 495, 66, 14);
		panel_1.add(lblDay);
		
		JLabel lblHour = new JLabel("Hour");
		lblHour.setBounds(10, 520, 66, 14);
		panel_1.add(lblHour);
		
		JLabel lblTeacherID = new JLabel("Teacher");
		lblTeacherID.setBounds(10, 545, 66, 14);
		panel_1.add(lblTeacherID);
		
		idcField = new JTextField();
		idcField.setBounds(59, 445, 110, 20);
		panel_1.add(idcField);
		idcField.setColumns(10);
		
		c_nameField = new JTextField();
		c_nameField.setBounds(59, 470, 110, 20);
		panel_1.add(c_nameField);
		c_nameField.setColumns(10);
		
		c_dayField = new JTextField();
		c_dayField.setBounds(59, 495, 110, 20);
		panel_1.add(c_dayField);
		c_dayField.setColumns(10);
		
		c_hourField = new JTextField();
		c_hourField.setBounds(59, 520, 110, 20);
		panel_1.add(c_hourField);
		c_hourField.setColumns(10);
		
		c_teacherField = new JTextField();
		c_teacherField.setBounds(59, 545, 110, 20);
		panel_1.add(c_teacherField);
		c_teacherField.setColumns(10);
		
		
		JButton btnAdd = new JButton("Create"); 
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client client = new Client("localhost", 8986); 
				Class c=new Class();
				c.setId(Integer.parseInt(idcField.getText()));
				c.setName(c_nameField.getText());
				c.setDay(c_dayField.getText());
				c.setHour(c_hourField.getText());
				c.setTeacher_id(Integer.parseInt(c_teacherField.getText()));
				client.addClass(c);
				try {
					client.connect();
					client.writeMessage("addClass:"+idcField.getText()+":"+c_nameField.getText()+":"
						+c_dayField.getText()+":"+c_hourField.getText()+":"+c_teacherField.getText());
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!"+ e1.getMessage());
				}
				
				init();
				
			}
		});
		btnAdd.setBounds(197, 545, 89, 23);
		panel_1.add(btnAdd);
		
		JButton btnUpdate_1 = new JButton("Update");
		btnUpdate_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
					Client client = new Client("localhost", 8986); 
					Class c=new Class();
					c.setId(Integer.parseInt(idcField.getText()));
					c.setName(c_nameField.getText());
					c.setDay(c_dayField.getText());
					c.setHour(c_hourField.getText());
					c.setTeacher_id(Integer.parseInt(c_teacherField.getText()));
					client.updateClass(c);
					try {
						client.connect();
						client.writeMessage("updateClass:"+idcField.getText()+":"+c_nameField.getText()+":"
								+c_dayField.getText()+":"+c_hourField.getText()+":"+c_teacherField.getText());
						client.readResponse();
					} catch (IOException e1) {
						System.err.println("Can't connect!" + e1.getMessage());
					}
					
					init();
				
			}
		});
		btnUpdate_1.setBounds(290, 545, 89, 23);
		panel_1.add(btnUpdate_1);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Client client = new Client("localhost", 8986); 
				client.deleteClass(Integer.parseInt(idcField.getText()));
				try {
					client.connect();
					client.writeMessage("deleteClass:"+idcField.getText());
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!" + e1.getMessage());
				}
				
				init();
			}
		});
		btnDelete.setBounds(383, 545, 89, 23);
		panel_1.add(btnDelete);
		
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Assign students to classes", null, panel_2, null);
		panel_2.setLayout(null);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(10, 11, 536, 128);
		panel_2.add(scrollPane_2);
		
		linkTable = new JTable();
		linkTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = linkTable.getSelectedRow();

				idlField.setText(linkTable.getModel().getValueAt(row, 0).toString());
				l_classField.setText(linkTable.getModel().getValueAt(row, 1).toString());
				l_studentField.setText(linkTable.getModel().getValueAt(row, 2).toString());
			}
		});
		scrollPane_2.setViewportView(linkTable);
		
		JLabel lblId_2 = new JLabel("ID");
		lblId_2.setBounds(10, 445, 66, 14);
		panel_2.add(lblId_2);
		
		JLabel lblClass = new JLabel("Class");
		lblClass.setBounds(10, 470, 66, 14);
		panel_2.add(lblClass);
		
		JLabel lblStudent = new JLabel("Student");
		lblStudent.setBounds(10, 495, 66, 14);
		panel_2.add(lblStudent);
		
		idlField = new JTextField();
		idlField.setBounds(59, 445, 110, 20);
		panel_2.add(idlField);
		idlField.setColumns(10);
		
		l_classField = new JTextField();
		l_classField.setBounds(59, 470, 110, 20);
		panel_2.add(l_classField);
		l_classField.setColumns(10);
		
		l_studentField = new JTextField();
		l_studentField.setBounds(59, 495, 110, 20);
		panel_2.add(l_studentField);
		l_studentField.setColumns(10);
		
		
		JButton btnAssign = new JButton("Assign"); 
		btnAssign.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client client = new Client("localhost", 8986); 
				Link l=new Link();
				l.setId(Integer.parseInt(idlField.getText()));
				l.setClass_id(Integer.parseInt(l_classField.getText()));
				l.setStudent_id(Integer.parseInt(l_studentField.getText()));
				client.addLink(l);
				try {
					client.connect();
					client.writeMessage("addLink:"+idlField.getText()+":"+l_classField.getText()+":"
						+l_studentField.getText());
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!"+ e1.getMessage());
				}
				
				init();
				
			}
		});
		btnAssign.setBounds(197, 545, 89, 23);
		panel_2.add(btnAssign);
		
		JButton btnEdit = new JButton("Edit"); 
		btnEdit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Client client = new Client("localhost", 8986); 
				Link l=new Link();
				l.setId(Integer.parseInt(idlField.getText()));
				l.setClass_id(Integer.parseInt(l_classField.getText()));
				l.setStudent_id(Integer.parseInt(l_studentField.getText()));
				client.updateLink(l);
				try {
					client.connect();
					client.writeMessage("updateLink:"+idlField.getText()+":"+l_classField.getText()+":"
						+l_studentField.getText());
					client.readResponse();
				} catch (IOException e1) {
					System.err.println("Can't connect!"+ e1.getMessage());
				}
				
				init();
				
			}
		});
		btnEdit.setBounds(290, 545, 89, 23);
		panel_2.add(btnEdit);
		
		
		
		init();
	}
	
	private void init() {
		Client client = new Client("localhost", 8986);
		try {
			client.connect();
			client.writeMessage("getUserList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!" + e.getMessage());
		}
		userTable.setModel(ViewTable.usersToTableModel(client.getUsers()));
		
		try {
			client.connect();
			client.writeMessage("getClassList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		classTable.setModel(ViewTable.classesToTableModel(client.getClasses()));
		
		try {
			client.connect();
			client.writeMessage("getLinkList");
			client.readResponse();
		} catch (IOException e) {
			System.err.println("Can't connect!"+ e.getMessage());
		}
		linkTable.setModel(ViewTable.linksToTableModel(client.getLinks()));
	}
}
