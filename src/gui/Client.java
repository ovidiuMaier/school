package gui;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Observable;

import model.*;
import model.Class;

public class Client {
	private String hostname;
	private int port;
	Socket socketClient;
	
	//private ArrayList<Class> classList= new ArrayList<Class>();
	private ArrayList<User> userList= new ArrayList<User>();
	private ArrayList<Class> classList= new ArrayList<Class>();
	private ArrayList<Class> teacherClassList= new ArrayList<Class>();
	private ArrayList<Class> studentClassList= new ArrayList<Class>();
	private ArrayList<Class> parentClassList= new ArrayList<Class>();
	private ArrayList<Link> linkList= new ArrayList<Link>();
	private ArrayList<Plink> plinkList= new ArrayList<Plink>();
	private ArrayList<Grade> gradeList= new ArrayList<Grade>();
	private ArrayList<Grade> studentGradeList= new ArrayList<Grade>();
	private ArrayList<Grade> parentGradeList= new ArrayList<Grade>();
	//private ArrayList<Patient> patientList= new ArrayList<Patient>();
	
	
	/*
	public ArrayList<Patient> getPatients(){
		return patientList;
	}
	
	*/
	public ArrayList<User> getUsers(){
		return userList;
	}
	
	public ArrayList<Class> getClasses(){
		return classList;
	}
	
	public ArrayList<Grade> getGrades(){
		return gradeList;
	}
	
	
	public ArrayList<Class> getTeacherClasses(String id){

		for(int i=0;i<classList.size();i++)
		{
			if (classList.get(i).getTeacher_id() == Integer.valueOf(id))
			{
				teacherClassList.add(classList.get(i));
			}
		}
		return teacherClassList;
	}
	
	public ArrayList<Class> getStudentClasses(String id){
		System.out.println("SIIIIIIIIIIIIIIIZE" + linkList.size());
		for(int i=0;i<linkList.size();i++)
		{
			if (linkList.get(i).getStudent_id() == Integer.valueOf(id))
			{
				for(int j=0;j<classList.size();j++)
				{
					if (classList.get(j).getId() == linkList.get(i).getClass_id()) 
						{
						System.out.println("student classssssssssss");
						studentClassList.add(classList.get(j));
						}
				}
				
			}
		}
		return studentClassList;
	}
	
	public ArrayList<Class> getParentClasses(String id){
		System.out.println("SIIIIIIIIIIIIIIIZE parent link" + plinkList.size());
		for(int i=0;i<plinkList.size();i++)
		{
			if (plinkList.get(i).getParent_id() == Integer.valueOf(id))
			{
				for(int j=0;j<linkList.size();j++)
				{
					if (linkList.get(j).getStudent_id() == plinkList.get(i).getStudent_id()) 
						{
						
							for(int k=0;k<classList.size();k++)
							{
								if (classList.get(k).getId() == linkList.get(j).getClass_id()) 
									{
									System.out.println("student classssssssssss");
									parentClassList.add(classList.get(k));
									}
							}
						

						}
				}
				
			}
		}
		return parentClassList;
	}
	
	
	
	public ArrayList<Grade> getStudentGrades(String id){

		System.out.println("GRAAAAAAADEE SIIIIIIZE" + gradeList.size());
		for(int i=0;i<gradeList.size();i++)
		{
			if (gradeList.get(i).getStudent_id() == Integer.valueOf(id))
			{
				studentGradeList.add(gradeList.get(i));
			}
		}
		return studentGradeList;
	}
	
	public ArrayList<Grade> getParentGrades(String id){

		System.out.println("SIIIIIIIIIIIIIIIZE parent link" + plinkList.size());
		for(int i=0;i<plinkList.size();i++)
		{
			if (plinkList.get(i).getParent_id() == Integer.valueOf(id))
			{
				for(int j=0;j<gradeList.size();j++)
				{
					if (gradeList.get(j).getStudent_id() == plinkList.get(i).getStudent_id()) 
						{
						parentGradeList.add(gradeList.get(j));
						
						}
				}
			}
		}
		return parentGradeList;
	}

	public ArrayList<Link> getLinks(){
		return linkList;
	}
	
	public ArrayList<Plink> getPlinks(){
		return plinkList;
	}


	
	/*
	public boolean checkAvailable(String date,int idD){
		for(int i=0;i<classList.size();i++)
		{
			System.out.println("check availability:"+classList.get(i).getDoctorId()+" "+idD +"&"+ classList.get(i).getDate()+" "+date);
			if(classList.get(i).getDoctorId()==idD && classList.get(i).getDate().equals(date))
				{
				System.out.println("Doctor not available");
				return false;
				}
		}
		return true;
	}
	*/
	public Client(String hostname, int port) {
		this.hostname = hostname;
		this.port = port;
	}

	public void connect() throws UnknownHostException, IOException {
		System.out.println("Attempting to connect to " + hostname + ":" + port);
		socketClient = new Socket(hostname, port);
		System.out.println("Connected");
	}

	public void readResponse() throws IOException {		
		String delims = ",";		
		
		String userInput;
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(socketClient.getInputStream()));
		System.out.println("Server response:");
		
		while ((userInput = stdIn.readLine()) != null) {
			System.out.println(userInput);			
			String[] tokens = userInput.split(delims);
			userInput=tokens[0];
			
			switch(userInput)
			{
			/*
			case "validSecretary":
				GUISecretary sgui=new GUISecretary();
				sgui.setVisible(true);
				break;
			case "validDoctor":
				GUIDoctor dgui=new GUIDoctor();
				dgui.setVisible(true);
				break;
				*/
			case "validAdmin":
				GUIAdmin agui=new GUIAdmin();
				agui.setVisible(true);
				break;
				
			case "validTeacher":
				GUITeacher tgui=new GUITeacher();
				tgui.setVisible(true);
				break;
				
			case "validStudent":
				GUIStudent sgui=new GUIStudent();
				sgui.setVisible(true);
				break;
				
			case "validParent":
				GUIParent pgui=new GUIParent();
				pgui.setVisible(true);
				break;
			/*	
			case "patientList":				
				for(int i=1;i<tokens.length;i++)
				{
					
					Patient p =new Patient();
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
							
					p.setId(Integer.parseInt(tok[0]));
					p.setName(tok[1]);
					p.setCNP(Integer.parseInt(tok[2]));
					p.setBirth(tok[3]);
					p.setAddress(tok[4]);
					
					patientList.add(p);	
				}
				break;
			
				*/
			
			case "userList":				
				for(int i=1;i<tokens.length;i++)
				{					
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
					User u =new User(tok[1],tok[2]);		
					u.setId(Integer.parseInt(tok[0]));
					u.setRole(tok[3]);				
					
					userList.add(u);
				}
				break;
		
			
			case "classList":				
				for(int i=1;i<tokens.length;i++)
				{					
					Class c =new Class();
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
							
					c.setId(Integer.parseInt(tok[0]));
					c.setName(tok[1]);
					c.setDay(tok[2]);
					c.setHour(tok[3]);
					c.setTeacher_id(Integer.parseInt(tok[4]));
					classList.add(c);
				}
				break;
				
			case "teacherClassList":				
				for(int i=1;i<tokens.length;i++)
				{					
					Class c =new Class();
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
							
					c.setId(Integer.parseInt(tok[0]));
					c.setName(tok[1]);
					c.setDay(tok[2]);
					c.setHour(tok[3]);
					c.setTeacher_id(Integer.parseInt(tok[4]));
					classList.add(c);
				}
				break;
				
			case "studentClassList":				
				for(int i=1;i<tokens.length;i++)
				{					
					Class c =new Class();
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
							
					c.setId(Integer.parseInt(tok[0]));
					c.setName(tok[1]);
					c.setDay(tok[2]);
					c.setHour(tok[3]);
					c.setTeacher_id(Integer.parseInt(tok[4]));
					classList.add(c);
				}
				break;
			
			case "linkList":				
				for(int i=1;i<tokens.length;i++)
				{					
					Link l =new Link();
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
							
					l.setId(Integer.parseInt(tok[0]));
					l.setClass_id(Integer.parseInt(tok[1]));
					l.setStudent_id(Integer.parseInt(tok[2]));
					
					linkList.add(l);
				}
				break;
				
			case "plinkList":				
				for(int i=1;i<tokens.length;i++)
				{					
					Plink l =new Plink();
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
							
					l.setId(Integer.parseInt(tok[0]));
					l.setStudent_id(Integer.parseInt(tok[1]));
					l.setParent_id(Integer.parseInt(tok[2]));
					
					plinkList.add(l);
				}
				break;
				
			case "studentGradeList":				
				for(int i=1;i<tokens.length;i++)
				{					
					Grade g =new Grade();
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
							
					g.setId(Integer.parseInt(tok[0]));
					g.setTeacher_id(Integer.parseInt(tok[1]));
					g.setStudent_id(Integer.parseInt(tok[2]));
					g.setClassname(tok[3]);
					g.setIs_final(Integer.parseInt(tok[4]));
					g.setValue(Integer.parseInt(tok[5]));
					
					gradeList.add(g);
				}
				break;
				
			case "gradeList":				
				for(int i=1;i<tokens.length;i++)
				{					
					Grade g =new Grade();
					String delim2 = "=";
					String[] tok = tokens[i].split(delim2);
							
					g.setId(Integer.parseInt(tok[0]));
					g.setTeacher_id(Integer.parseInt(tok[1]));
					g.setStudent_id(Integer.parseInt(tok[2]));
					g.setClassname(tok[3]);
					g.setIs_final(Integer.parseInt(tok[4]));
					g.setValue(Integer.parseInt(tok[5]));
					
					gradeList.add(g);
				}
				break;
				
				
			
			case "patientArrived":
				Watched p=Watched.getInstance();
				p.arrived();
				break;
			
			}
			
		}
	}

	public void closeConnection() throws IOException {
		socketClient.close();
	}

	public void writeMessage(String message) throws IOException {
		String process = message + (char) 13;
		BufferedWriter stdOut = new BufferedWriter(new OutputStreamWriter(socketClient.getOutputStream()));
		stdOut.write(process);
		stdOut.flush();
	}
/*
	public void addPatient(Patient p) {
		patientList.add(p);		
	}

	public void updatePatient(Patient p) {
		for(int i=0;i<patientList.size();i++)
			if(p.getId() == patientList.get(i).getId())
			{
				patientList.get(i).setName(p.getName());
				patientList.get(i).setCNP(p.getCNP());
				patientList.get(i).setBirth(p.getBirth());
				patientList.get(i).setAddress(p.getAddress());
			}
		
	}
*/
	public void addClass(Class c) {
		classList.add(c);		
	}

	public void updateClass(Class c) {
		for(int i=0;i<classList.size();i++)
			if(c.getId() == classList.get(i).getId())
			{
				classList.get(i).setName(c.getName());
				classList.get(i).setDay(c.getDay());
				classList.get(i).setHour(c.getHour());
				classList.get(i).setTeacher_id(c.getTeacher_id());
			}
		
	}

	public void deleteClass(int id) {
		ArrayList<Class> newList=new ArrayList<Class>();
		for(int i=0;i<classList.size();i++)
			if(id != classList.get(i).getId())
				newList.add(classList.get(i));
		
		classList=newList;
		
	}
	
	public void addLink(Link l) {
		linkList.add(l);		
	}
	
	public void addPlink(Plink p) {
		plinkList.add(p);		
	}
	
	
	public void updateLink(Link l) {
		for(int i=0;i<linkList.size();i++)
			if(l.getId() == linkList.get(i).getId())
			{
				linkList.get(i).setClass_id(l.getClass_id());
				linkList.get(i).setStudent_id(l.getStudent_id());
				
			}
		
	}
	
	public void addGrade(Grade g) {
		gradeList.add(g);		
	}
	
	public void updateGrade(Grade g) {
		for(int i=0;i<gradeList.size();i++)
			if(g.getId() == gradeList.get(i).getId())
			{
				gradeList.get(i).setTeacher_id(g.getTeacher_id());
				gradeList.get(i).setStudent_id(g.getStudent_id());
				gradeList.get(i).setClassname(g.getClassname());
				gradeList.get(i).setIs_final(g.getIs_final());
				gradeList.get(i).setValue(g.getValue());
				
			}
		
	}
	
	public void addUser(User u) {
		userList.add(u);		
	}

	public void updateUser(User u) {
		for(int i=0;i<userList.size();i++)
			if(u.getId() == userList.get(i).getId() && u.getRole()==userList.get(i).getRole())
			{
				userList.get(i).setUsername(u.getUsername());
				userList.get(i).setPassword(u.getPassword());
				
			}
	}

	public void deleteUser(User u) {
		ArrayList<User> newList=new ArrayList<User>();
		
		for(int i=0;i<userList.size();i++)
			if(!(u.getId() == userList.get(i).getId() && u.getRole()==userList.get(i).getRole()))
			{
				newList.add(userList.get(i));				
			}
		
		userList=newList;
		
	}
	
}