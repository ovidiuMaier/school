package dao;
import java.io.*;
import java.net.*;
import model.*;

public class Server implements Runnable {
	private Socket connection;
	private int ID;

	public Server(Socket connection, int ID) {
		this.connection = connection;
		this.ID = ID;
	}

	public void run() {
		try {
			readFromClient(connection);
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			try {
				connection.close();
			} catch (IOException e) {
			}
		}
	}

	public void readFromClient(Socket client) throws Exception {
		BufferedInputStream is = new BufferedInputStream(
				client.getInputStream());
		InputStreamReader reader = new InputStreamReader(is);
		StringBuffer process = new StringBuffer();
		int character;
		while ((character = reader.read()) != 13) {
			process.append((char) character);
		}
		System.out.println("readfromclient " + process);


		String returnCode = null,op, toBeParsed = process + "";
		String delims = ":";
		String[] tokens = toBeParsed.split(delims);
		op = tokens[0];
		
		TeacherDao teacher_dao = new TeacherDao();
		StudentDao student_dao = new StudentDao();
		AdminDao admin_dao = new AdminDao();
		ParentDao parent_dao = new ParentDao();
		
		
		switch (op) {
		/*
		case "teacherLogin":
			if(teacher_dao.loginTeacher(new User(tokens[1], tokens[2])))
				returnCode="validTeacher"+ (char) 13;
			else 
				returnCode="invalidTeacher"+ (char) 13;			
			break;
		case "doctorLogin":
			if(doctor_dao.loginStudent(new User(tokens[1], tokens[2])))
				returnCode="validStudent"+ (char) 13;
			else 
				returnCode="invalidStudent"+ (char) 13;			
			break;
			*/
		case "adminLogin":
			
			if(admin_dao.loginAdmin(new User(tokens[1], tokens[2])))
				returnCode="validAdmin"+ (char) 13;
			else 
				returnCode="invalidAdmin"+ (char) 13;			
			break;
			
		case "teacherLogin":
			
			if(teacher_dao.loginTeacher(new User(tokens[1], tokens[2])))
				returnCode="validTeacher"+ (char) 13;
			else 
				returnCode="invalidTeacher"+ (char) 13;			
			break;
			
		case "studentLogin":
			
			if(student_dao.loginStudent(new User(tokens[1], tokens[2])))
				returnCode="validStudent"+ (char) 13;
			else 
				returnCode="invalidStudent"+ (char) 13;			
			break;
			
		case "parentLogin":
			
			if(parent_dao.loginParent(new User(tokens[1], tokens[2])))
				returnCode="validParent"+ (char) 13;
			else 
				returnCode="invalidParent"+ (char) 13;			
			break;
		/*	
		case "getPatientList":
			   returnCode= teacher_dao.getPatientList();
			break;
		*/
		case "getClassList":   
			returnCode= admin_dao.getClassList();
			   
			break;
		case "getLinkList":   
			returnCode= admin_dao.getLinkList();
			   
			break;
			
		case "getPlinkList":   
			returnCode= parent_dao.getPlinkList();
			   
			break;
			
		case "getGradeList":   
			returnCode= teacher_dao.getGradeList();
			   
			break;
		
			
			/*
		case "addPatient":
			teacher_dao.addPatient(tokens[1],tokens[2],tokens[3],tokens[4],tokens[5]);
			break;
		case "updatePatient":
			teacher_dao.updatePatient(tokens[1],tokens[2],tokens[3],tokens[4],tokens[5]);
			break;
			*/
		case "addClass":
			admin_dao.addClass(tokens[1],tokens[2],tokens[3],tokens[4],tokens[5]);		
			break;
		case "updateClass":
			admin_dao.updateClass(tokens[1],tokens[2],tokens[3],tokens[4],tokens[5]);		
			break;
		case "deleteClass":
			admin_dao.deleteClass(tokens[1]);		
			break;
			
		case "viewTeacherClass":
			teacher_dao.getClassList(tokens[1]);		
			break;
			
		case "addLink":
			admin_dao.addLink(tokens[1],tokens[2],tokens[3]);		
			break;
		case "updateLink":
			admin_dao.updateLink(tokens[1],tokens[2],tokens[3]);		
			break;
			
		case "addGrade":
			teacher_dao.addGrade(tokens[1],tokens[2],tokens[3],tokens[4],tokens[5],tokens[6]);		
			break;
		case "updateGrade":
			teacher_dao.updateGrade(tokens[1],tokens[2],tokens[3],tokens[4],tokens[5],tokens[6]);		
			break;
			
		case "getUserList":
			   returnCode= admin_dao.getUserList();
			break;
		case "addTeacher":
			admin_dao.addTeacher(tokens[1],tokens[2],tokens[3]);		
			break;
		case "addStudent":
			admin_dao.addStudent(tokens[1],tokens[2],tokens[3]);		
			break;
		case "addAdmin":
			admin_dao.addAdmin(tokens[1],tokens[2],tokens[3]);		
			break;
		case "updateTeacher":
			admin_dao.updateTeacher(tokens[1],tokens[2],tokens[3]);		
			break;
		case "updateStudent":
			admin_dao.updateStudent(tokens[1],tokens[2],tokens[3]);		
			break;	
		case "deleteTeacher":
			admin_dao.deleteTeacher(tokens[1]);		
			break;	
		case "deleteStudent":
			admin_dao.deleteStudent(tokens[1]);		
			break;
		case "arrived":
			returnCode= "patientArrived";
			break;	
		}
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
		}
		sendMessage(client, returnCode);
	}

	private void sendMessage(Socket client, String message) throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
				client.getOutputStream()));
		writer.write(message);
		writer.flush();
	}

	public static void main(String[] args) {
		int port = 8986;
		int number = 0;
		System.out.println("Server started at port " + port);

		try {
			ServerSocket serverSocket = new ServerSocket(port);
			System.out.println("Server Initialized");
		
			while (true) {
				Socket client = serverSocket.accept();
				Runnable runnable = new Server(client, ++number);
				Thread thread = new Thread(runnable);
				thread.start();
			}
		} catch (Exception e) {
		}
	}
}