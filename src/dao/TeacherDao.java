package dao;

import java.util.*;
import javax.swing.*;

import java.io.FileInputStream;
import java.sql.*;

import model.*;


public class TeacherDao {

    private Connection myConn;
    private ResultSet result_set = null;
    
    public TeacherDao() throws Exception {
		
		// get db properties
		Properties props = new Properties();
		props.load(new FileInputStream("school.properties"));
		
		String user = props.getProperty("user");
		String password = props.getProperty("password");
		String dburl = props.getProperty("dburl");
		
		// connect to database
		myConn = DriverManager.getConnection(dburl, user, password);
		
		System.out.println("Teacher DAO - DB connection successful to: " + dburl);
	}
    
    public boolean loginTeacher(User u) {
    	PreparedStatement myStmt = null;
        try {
        	myStmt = myConn.prepareStatement("select * from teacher where email=? and password=?");
            myStmt.setString(1, u.getUsername());
            myStmt.setString(2, u.getPassword());
            
            result_set = myStmt.executeQuery();

            if (result_set.next()) {
            	result_set.close();
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Invalid credentials!");
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        return false;
    }
    
    public String getClassList(String id) {
		String cList = "teacherClassList";
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from class where teacher_id=?");
			myStmt.setInt(1,Integer.parseInt(id));
			result_set = myStmt.executeQuery();
			while (result_set.next()) {
				cList = cList + "," + result_set.getInt("class_id") + "="
						+ result_set.getString("name") + "=" + result_set.getString("day")
						+ "=" + result_set.getString("hour") + "=" + result_set.getString("teacher_id");
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "getClassList " + e);

		} finally {
			try {
				DAOUtils.close(myStmt, result_set);
			} catch (Exception e) {
			}
		}

		return cList;
	}
    
    public String getGradeList() {
		String gList = "gradeList";
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from grade");
			result_set = myStmt.executeQuery();
			while (result_set.next()) {
				gList = gList + "," + result_set.getInt("grade_id") + "="
						+ result_set.getInt("teacher_id") + "=" + result_set.getInt("student_id") + "=" + result_set.getString("classname") + "=" + result_set.getInt("is_final") + "=" + result_set.getInt("value");
						
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "getGradeList " + e);

		} finally {
			try {
				DAOUtils.close(myStmt, result_set);
			} catch (Exception e) {
			}
		}

		return gList;
	}
    
    public void addGrade(String id, String teacher_id, String student_id, String classname, String is_final, String value) {
		PreparedStatement myStmt = null;
	      try {     
	          myStmt = myConn.prepareStatement("insert into grade values(?,?,?,?,?,?)");
	          myStmt.setInt(1,Integer.parseInt(id));
	          myStmt.setInt(2,Integer.parseInt(teacher_id));
	          myStmt.setInt(3,Integer.parseInt(student_id));
	          myStmt.setString(4,classname);
	          myStmt.setInt(5,Integer.parseInt(is_final));
	          myStmt.setInt(6,Integer.parseInt(value));
	   
	          myStmt.executeUpdate();
	          

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }
	}

	public void updateGrade(String id, String teacher_id, String student_id, String classname, String is_final, String value) {
		 PreparedStatement myStmt = null;
		 try {     
	          myStmt = myConn.prepareStatement("update grade set teacher_id=? , student_id=?, classname=?, is_final=?, value=? where grade_id=?");
	          myStmt.setInt(1,Integer.parseInt(teacher_id));
	          myStmt.setInt(2,Integer.parseInt(student_id));
	          myStmt.setString(3,classname);
	          myStmt.setInt(4,Integer.parseInt(is_final));
	          myStmt.setInt(5,Integer.parseInt(value));
	          myStmt.setInt(6,Integer.parseInt(id));
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }
	}
	
}
