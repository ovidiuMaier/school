package dao;

import java.util.*;
import javax.swing.*;

import java.io.FileInputStream;
import java.sql.*;

import model.*;


public class AdminDao {

    private Connection myConn;
    private ResultSet result_set = null;
    
    public AdminDao() throws Exception {
		
		// get db properties
		Properties props = new Properties();
		props.load(new FileInputStream("school.properties"));
		
		String user = props.getProperty("user");
		String password = props.getProperty("password");
		String dburl = props.getProperty("dburl");
		
		// connect to database
		myConn = DriverManager.getConnection(dburl, user, password);
		
		System.out.println("Admin DAO - DB connection successful to: " + dburl);
	}
    
    public boolean loginAdmin(User u) {
    	PreparedStatement myStmt = null;
        try {
        	myStmt = myConn.prepareStatement("select * from admin where email=? and password=?");
            myStmt.setString(1, u.getUsername());
            myStmt.setString(2, u.getPassword());
            
            result_set = myStmt.executeQuery();

            if (result_set.next()) {
            	result_set.close();
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Invalid credentials!");
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        return false;
    }
    
    public void addAdmin(String id, String user, String pass) {
		PreparedStatement myStmt = null;
	      try {    
	          myStmt = myConn.prepareStatement("insert into admin values(?,?,?)");
	          
	          myStmt.setInt(1,Integer.parseInt(id));
	          myStmt.setString(2, user);
	          myStmt.setString(3, pass);
	        
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	          System.out.println("createA " + e);

	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }		
	}
    
    public void addStudent(String id, String user, String pass) {
		PreparedStatement myStmt = null;
	      try {     
	          myStmt = myConn.prepareStatement("insert into student values(?,?,?)");
	          
	          myStmt.setInt(1,Integer.parseInt(id));
	          myStmt.setString(2, user);
	          myStmt.setString(3, pass);
	        
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	          System.out.println("createD " + e);

	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }		
	}

    public void updateStudent(String id, String user, String pass) {
		PreparedStatement myStmt = null;
	      try {     
	          
	          myStmt = myConn.prepareStatement("update student set email=? , password=?  where student_id=?");	          
	          
	          myStmt.setString(1, user);
	          myStmt.setString(2, pass);
	          myStmt.setInt(3,Integer.parseInt(id));
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	          System.out.println("upD " + e);

	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }			
	}
    
    public void deleteStudent(String id) {
		 PreparedStatement myStmt = null;
	       try {     
	           myStmt = myConn.prepareStatement("delete from student where student_id=?");
	           myStmt.setInt(1,Integer.parseInt(id));
	           myStmt.executeUpdate();                   

	       } catch (Exception e) {
	           JOptionPane.showMessageDialog(null, e);
	           System.out.println("delD " + e);

	       } finally {
	           try {
	        	   DAOUtils.close(myStmt);
	           } catch (Exception e) {
	           }
	       }
	}
    
	public void addTeacher(String id, String user, String pass) {
		PreparedStatement myStmt = null;
      try {     
          myStmt = myConn.prepareStatement("insert into teacher values(?,?,?)");
          
          myStmt.setInt(1,Integer.parseInt(id));
          myStmt.setString(2, user);
          myStmt.setString(3, pass);
          
          myStmt.executeUpdate();

      } catch (Exception e) {
          JOptionPane.showMessageDialog(null, e);
          System.out.println("createS " + e);

      } finally {
          try {
        	  DAOUtils.close(myStmt);
          } catch (Exception e) {
          }
      }		
	}

	public void updateTeacher(String id, String user, String pass) {
		PreparedStatement myStmt = null;
	      try {     
	          myStmt = myConn.prepareStatement("update teacher set email=? , password=?  where teacher_id=?");	          
	          
	          myStmt.setString(1, user);
	          myStmt.setString(2, pass);
	          myStmt.setInt(3,Integer.parseInt(id));
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	          System.out.println("upA " + e);

	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }			
	}

	public void deleteTeacher(String id) {
		PreparedStatement myStmt = null;
	       try {     
	           myStmt = myConn.prepareStatement("delete from teacher where teacher_id=?");
	           myStmt.setInt(1,Integer.parseInt(id));
	           myStmt.executeUpdate();                   

	       } catch (Exception e) {
	           JOptionPane.showMessageDialog(null, e);
	           System.out.println("delS " + e);

	       } finally {
	           try {
	        	   DAOUtils.close(myStmt);
	           } catch (Exception e) {
	           }
	       }

		
	}
	
	public String getUserList() {
		String uList = "userList";
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from teacher");
			result_set = myStmt.executeQuery();

			while (result_set.next()) {
				uList = uList + "," + result_set.getInt("teacher_id") + "="
						+ result_set.getString("email") + "=" + result_set.getString("password")
						+ "=teacher";
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		} finally {
			try {
				DAOUtils.close(myStmt, result_set);
			} catch (Exception e) {
			}
		}	
			try {
				myStmt = myConn.prepareStatement("select * from student");
				result_set = myStmt.executeQuery();

				while (result_set.next()) {
					uList = uList + "," + result_set.getInt("student_id") + "="
							+ result_set.getString("email") + "=" + result_set.getString("password")
							+ "=student";
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e);

			} finally {
				try {
					DAOUtils.close(myStmt, result_set);
				} catch (Exception e) {
				}
			}
			
		return uList;	
	}
	
	public String getClassList() {
		String cList = "classList";
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from class");
			result_set = myStmt.executeQuery();
			while (result_set.next()) {
				cList = cList + "," + result_set.getInt("class_id") + "="
						+ result_set.getString("name") + "=" + result_set.getString("day")
						+ "=" + result_set.getString("hour") + "=" + result_set.getString("teacher_id");
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "getClassList " + e);

		} finally {
			try {
				DAOUtils.close(myStmt, result_set);
			} catch (Exception e) {
			}
		}

		return cList;
	}
	
	public void addClass(String id, String name, String day,String hour, String teacher_id) {
		PreparedStatement myStmt = null;
	      try {     
	          myStmt = myConn.prepareStatement("insert into class values(?,?,?,?,?)");
	          myStmt.setInt(1,Integer.parseInt(id));
	          myStmt.setString(2,name);
	          myStmt.setString(3,day);
	          myStmt.setString(4, hour);
	          myStmt.setInt(5,Integer.parseInt(teacher_id));
	          myStmt.executeUpdate();
	          

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }
	}

	public void updateClass(String id, String name, String day, String hour, String teacher_id) {
		 PreparedStatement myStmt = null;
		 try {     
	          myStmt = myConn.prepareStatement("update class set name=? , day=? , hour=?, teacher_id=? where class_id=?");
	          myStmt.setString(1, name);
	          myStmt.setString(2, day);
	          myStmt.setString(3, hour);
	          myStmt.setInt(4,Integer.parseInt(teacher_id));
	          myStmt.setInt(5,Integer.parseInt(id));
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }
	}

	public void deleteClass(String id) {
		 PreparedStatement myStmt = null;
       try {     
           myStmt = myConn.prepareStatement("delete from class where class_id=?");
           myStmt.setInt(1,Integer.parseInt(id));
           myStmt.executeUpdate();                   
       } catch (Exception e) {
           JOptionPane.showMessageDialog(null, e);
       } finally {
           try {
        	   DAOUtils.close(myStmt);
           } catch (Exception e) {
           }
       }
	}
	
	public String getLinkList() {
		String lList = "linkList";
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from link");
			result_set = myStmt.executeQuery();
			while (result_set.next()) {
				lList = lList + "," + result_set.getInt("link_id") + "="
						+ result_set.getInt("class_id") + "=" + result_set.getInt("student_id");
						
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "getLinkList " + e);

		} finally {
			try {
				DAOUtils.close(myStmt, result_set);
			} catch (Exception e) {
			}
		}

		return lList;
	}
	
	public void addLink(String id, String class_id, String student_id) {
		PreparedStatement myStmt = null;
	      try {     
	          myStmt = myConn.prepareStatement("insert into link values(?,?,?)");
	          myStmt.setInt(1,Integer.parseInt(id));
	          myStmt.setInt(2,Integer.parseInt(class_id));
	          myStmt.setInt(3,Integer.parseInt(student_id));
	          myStmt.executeUpdate();
	          

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }
	}

	public void updateLink(String id, String class_id, String student_id) {
		 PreparedStatement myStmt = null;
		 try {     
	          myStmt = myConn.prepareStatement("update link set class_id=? , student_id=? where link_id=?");
	          myStmt.setInt(1,Integer.parseInt(class_id));
	          myStmt.setInt(2,Integer.parseInt(student_id));
	          myStmt.setInt(3,Integer.parseInt(id));
	          myStmt.executeUpdate();         

	      } catch (Exception e) {
	          JOptionPane.showMessageDialog(null, e);
	      } finally {
	          try {
	        	  DAOUtils.close(myStmt);
	          } catch (Exception e) {
	          }
	      }
	}
	

}
