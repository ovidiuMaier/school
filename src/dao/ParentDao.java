package dao;

import java.util.*;
import javax.swing.*;

import java.io.FileInputStream;
import java.sql.*;

import model.*;


public class ParentDao {

    private Connection myConn;
    private ResultSet result_set = null;
    
    public ParentDao() throws Exception {
		
		// get db properties
		Properties props = new Properties();
		props.load(new FileInputStream("school.properties"));
		
		String user = props.getProperty("user");
		String password = props.getProperty("password");
		String dburl = props.getProperty("dburl");
		
		// connect to database
		myConn = DriverManager.getConnection(dburl, user, password);
		
		System.out.println("Parent DAO - DB connection successful to: " + dburl);
	}
    
    public boolean loginParent(User u) {
    	PreparedStatement myStmt = null;
        try {
        	myStmt = myConn.prepareStatement("select * from parent where email=? and password=?");
            myStmt.setString(1, u.getUsername());
            myStmt.setString(2, u.getPassword());
            
            result_set = myStmt.executeQuery();

            if (result_set.next()) {
            	result_set.close();
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Invalid credentials!");
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

        return false;
    }
    
    public String getClassList(String id) {
		String cList = "studentClassList";
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from class where student_id=?");
			myStmt.setInt(1,Integer.parseInt(id));
			result_set = myStmt.executeQuery();
			while (result_set.next()) {
				cList = cList + "," + result_set.getInt("class_id") + "="
						+ result_set.getString("name") + "=" + result_set.getString("day")
						+ "=" + result_set.getString("hour") + "=" + result_set.getString("student_id");
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "getClassList " + e);

		} finally {
			try {
				DAOUtils.close(myStmt, result_set);
			} catch (Exception e) {
			}
		}

		return cList;
	}
    
    public String getGradeList() {
		String gList = "gradeList";
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from grade");
			result_set = myStmt.executeQuery();
			while (result_set.next()) {
				gList = gList + "," + result_set.getInt("grade_id") + "="
						+ result_set.getInt("student_id") + "=" + result_set.getInt("student_id") + "=" + result_set.getString("classname") + "=" + result_set.getInt("is_final") + "=" + result_set.getInt("value");
						
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "getGradeList " + e);

		} finally {
			try {
				DAOUtils.close(myStmt, result_set);
			} catch (Exception e) {
			}
		}

		return gList;
	}
   
    
    public String getPlinkList() {
		String lList = "plinkList";
		PreparedStatement myStmt = null;
		try {
			myStmt = myConn.prepareStatement("select * from plink");
			result_set = myStmt.executeQuery();
			while (result_set.next()) {
				lList = lList + "," + result_set.getInt("plink_id") + "="
						+ result_set.getInt("student_id") + "=" + result_set.getInt("parent_id");
						
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "getPLinkList " + e);

		} finally {
			try {
				DAOUtils.close(myStmt, result_set);
			} catch (Exception e) {
			}
		}

		return lList;
	}
}
