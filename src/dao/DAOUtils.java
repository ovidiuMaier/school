package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DAOUtils {

	public static void close(Connection myConn, Statement myStmt, ResultSet result_set)
			throws SQLException {

		if (result_set != null) {
			result_set.close();
		}

		if (myStmt != null) {
			
		}
		
		if (myConn != null) {
			myConn.close();
		}
	}

	public static void close(Statement myStmt, ResultSet result_set) throws SQLException {
		close(null, myStmt, result_set);		
	}

	public static void close(Statement myStmt) throws SQLException {
		close(null, myStmt, null);		
	}
	
}
