package model;

import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class ViewTable {
	/*
	 public static DefaultTableModel patientsToTableModel(ArrayList<Patient> patients) {
	        Vector<String> columnNames = new Vector<String>();
	        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
	        columnNames.add("ID");
	        columnNames.add("Name");
	        columnNames.add("CNP");
	        columnNames.add("Date of Birth");
	        columnNames.add("Address");

	        for (int i = 0; i < patients.size(); i++) {
	        	Patient b = patients.get(i);
	            Vector<Object> row = new Vector<Object>(); 
	            
	            row.add(b.getId());
	            row.add(b.getName());
	            row.add(b.getCNP());
	            row.add(b.getBirth());
	            row.add(b.getAddress());
	            
	            rows.add(row);
	        }

	        DefaultTableModel model = new DefaultTableModel(rows, columnNames) {

	            public boolean isCellEditable(int row, int column) {
	                return false;
	            }
	        };
	        return model;
	    }
	 
	 public static DefaultTableModel consultationsToTableModel(ArrayList<Consultation> cons) {
	        Vector<String> columnNames = new Vector<String>();
	        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
	        columnNames.add("ID");
	        columnNames.add("Patient ID");
	        columnNames.add("Doctor ID");
	        columnNames.add("Date");

	        for (int i = 0; i < cons.size(); i++) {
	            Consultation c = cons.get(i);
	            Vector<Object> row = new Vector<Object>(); 
	            row.add(c.getId());
	            row.add(c.getPatientId());
	            row.add(c.getDoctorId());
	            row.add(c.getDate());
	            rows.add(row);
	        }

	        DefaultTableModel model = new DefaultTableModel(rows, columnNames) {

	            public boolean isCellEditable(int row, int column) {
	                return false;
	            }
	        };
	        return model;
	    }
	 	 */
	public static DefaultTableModel classesToTableModel(ArrayList<Class> cls) {
        Vector<String> columnNames = new Vector<String>();
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
        columnNames.add("ID");
        columnNames.add("Name");
        columnNames.add("Day");
        columnNames.add("Hour");
        columnNames.add("Teacher");

        for (int i = 0; i < cls.size(); i++) {
            Class c = cls.get(i);
            Vector<Object> row = new Vector<Object>(); 
            row.add(c.getId());
            row.add(c.getName());
            row.add(c.getDay());
            row.add(c.getHour());
            row.add(c.getTeacher_id());
            rows.add(row);
        }

        DefaultTableModel model = new DefaultTableModel(rows, columnNames) {

            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        return model;
    }
	
	public static DefaultTableModel linksToTableModel(ArrayList<Link> lnk) {
        Vector<String> columnNames = new Vector<String>();
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
        columnNames.add("ID");
        columnNames.add("Class");
        columnNames.add("Student");

        for (int i = 0; i < lnk.size(); i++) {
            Link c = lnk.get(i);
            Vector<Object> row = new Vector<Object>(); 
            row.add(c.getId());
            row.add(c.getClass_id());
            row.add(c.getStudent_id());
            rows.add(row);
        }

        DefaultTableModel model = new DefaultTableModel(rows, columnNames) {

            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        return model;
    }
	
	public static DefaultTableModel plinksToTableModel(ArrayList<Plink> plnk) {
        Vector<String> columnNames = new Vector<String>();
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
        columnNames.add("ID");
        columnNames.add("Student");
        columnNames.add("Parent");

        for (int i = 0; i < plnk.size(); i++) {
            Plink c = plnk.get(i);
            Vector<Object> row = new Vector<Object>(); 
            row.add(c.getId());
            row.add(c.getStudent_id());
            row.add(c.getParent_id());
            rows.add(row);
        }

        DefaultTableModel model = new DefaultTableModel(rows, columnNames) {

            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        return model;
    }
	
	
	public static DefaultTableModel gradesToTableModel(ArrayList<Grade> grd) {
        Vector<String> columnNames = new Vector<String>();
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
        columnNames.add("ID");
        columnNames.add("Teacher");
        columnNames.add("Student");
        columnNames.add("Class");
        columnNames.add("Final");
        columnNames.add("Value");

        for (int i = 0; i < grd.size(); i++) {
            Grade g = grd.get(i);
            Vector<Object> row = new Vector<Object>(); 
            row.add(g.getId());
            row.add(g.getTeacher_id());
            row.add(g.getStudent_id());
            row.add(g.getClassname());
            row.add(g.getIs_final());
            row.add(g.getValue());
            rows.add(row);
        }

        DefaultTableModel model = new DefaultTableModel(rows, columnNames) {

            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        return model;
    }


	 public static DefaultTableModel usersToTableModel(ArrayList<User> users) {
	        Vector<String> columnNames = new Vector<String>();
	        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
	        columnNames.add("ID");
	        columnNames.add("Email");
	        columnNames.add("Password");
	        columnNames.add("Role");

	        for (int i = 0; i < users.size(); i++) {
	            User u = users.get(i);
	            Vector<Object> row = new Vector<Object>(); 
	            row.add(u.getId());
	            row.add(u.getUsername());
	            row.add(u.getPassword());
	            row.add(u.getRole());
	            rows.add(row);
	        }

	        DefaultTableModel model = new DefaultTableModel(rows, columnNames) {

	            public boolean isCellEditable(int row, int column) {
	                return false;
	            }
	        };
	        return model;
	    }

}
