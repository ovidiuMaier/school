package model;

import java.util.Observable;

import gui.*;

public class Watched extends Observable {
	
	private static Watched instance = null;
	public static synchronized Watched getInstance()
	{
		if (instance == null) instance = new Watched(); 
		return instance;
	}
	
	public void arrived(){
		setChanged();
		notifyObservers();
	}

}
