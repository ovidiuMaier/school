package model;

public class Grade {
	private int id;
	private int teacher_id;
	private int student_id;
	private String classname;
	private int is_final;
	private int value;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTeacher_id() {
		return teacher_id;
	}
	public void setTeacher_id(int teacher_id) {
		this.teacher_id = teacher_id;
	}
	public int getStudent_id() {
		return student_id;
	}
	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}
	public String getClassname() {
		return classname;
	}
	public void setClassname(String classname) {
		this.classname = classname;
	}
	public int getIs_final() {
		return is_final;
	}
	public void setIs_final(int is_final) {
		this.is_final = is_final;
	}
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "Class [id=" + id + ", teacher_id=" + teacher_id + ", student_id=" + student_id + ", classname=" + classname + ", is_final=" + is_final + ", value=" + value + "]";
	}
	
	
	
}
