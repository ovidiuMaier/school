package model;

public class User {
    
	private int id;
	private String email;
    private String password;
    private String role;
   
    public User(String email,String password)
    {
        this.email=email;
        this.password=password;
    }
    
    public int getId()
    {
    	return id;
    }
    
    public void setId(int i)
    {
    	this.id=i;
    }
      
    public String getUsername()
    {
      return this.email;
    }
   
    public void setUsername(String email2)
    {
 	   this.email=email2;	
    }

    public String getRole()
    {
    	return role;
    }
    
    public void setRole(String role)
   {
	   this.role=role;	
   }

   
   public String getPassword()
   {
      return this.password;
   }
   
   public void setPassword(String password2)
   {
	   this.password=password2;	
   }
    
}
