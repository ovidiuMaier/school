package model;

public class Class {
	private int id;
	private String name;
    private String day;
    private String hour;
    private int teacher_id;
	
    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getHour() {
		return hour;
	}
	public void setHour(String hour) {
		this.hour = hour;
	}
	
	public int getTeacher_id() {
		return teacher_id;
	}
	public void setTeacher_id(int teacher_id) {
		this.teacher_id = teacher_id;
	}
	
	
	@Override
	public String toString() {
		return "Class [id=" + id + ", name=" + name + ", day=" + day + ", hour=" + hour + ", teacher_id=" + teacher_id + "]";
	}
	

}
