package model;

public class Plink {
	private int id;
	private int student_id;
	private int parent_id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStudent_id() {
		return student_id;
	}
	public void setStudent_id(int student_id) {
		this.student_id = student_id;
	}
	public int getParent_id() {
		return parent_id;
	}
	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}
	
	@Override
	public String toString() {
		return "Plink [id=" + id + ", student_id=" + student_id + ", parent_id=" + parent_id + "]";
	}
	
}
